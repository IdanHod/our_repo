#include "student.h"

Course** Student::getCourses()
{
	return _Courses;
}

void Student::init(string name, Course** courses, int crsCount)
{
	_name = name;
	_Courses = courses;
	_crsCount = crsCount;



}
string Student::getName()
{
	return _name;
}
void Student::setName(string name)
{
	_name = name;
}

double Student::getAvg()
{
	double sum = 0;
	for (int i = 0; i < _crsCount; i++)
	{
		sum += _Courses[i]->getFinalGrade();

	}

	return sum / _crsCount;
}

int Student::getCrsCount()
{
	return _crsCount;
}


Student* newStudent()
{
	string name;
	int crsCount;

	cout << "Enter student's name: ";
	cin >> name;
	cout << "Enter number of courses for student: ";
	cin >> crsCount;

	Course* crs = NULL;

	// handle of courses list
	Course** courses = new Course*[crsCount];
	for (int i = 0; i < crsCount; i++)
	{
		//Create new course
		string crsName;
		int test1, test2, exam;
		cout << "Enter Course name: ";
		cin >> crsName;
		cout << "Enter first test grade: ";
		cin >> test1;
		cout << "Enter second test grade: ";
		cin >> test2;
		cout << "Enter exam grade: ";
		cin >> exam;
		cout << endl;

		// create course object
		// TO DO: should create new Course object and assign it to crs 
		crs = new Course();

		// call to init function of course object
		crs->init(crsName, test1, test2, exam);

		//Add course to list
		courses[i] = crs;
	}


	Student* student;

	// create student object
	// TO DO: create new student object
	student = new Student();

	// call to init function of student object
	// TO DO: call the init function of students object
	student->init(name, courses, crsCount);


	// return student object
	return student;

}