/* version 1.0 of my school*/

#include <iostream>
#include <string>
#include "course.h"
#include "student.h"
#define MAX_STUDENTS 10

#define MENU_STUDENT_DETAILS 1
#define MENU_AVERAGE 2
#define MENU_PRINT_COURSES 3
#define MENU_EXIT 4

using namespace std;







int showMenu()
{
	int option = 0;
	cout << "Welcome to Magshimim! You have the following options:" << endl;
	cout << "1. Enter student deatails and courses" << endl;
	cout << "2. Calculate average grade of all courses" << endl;
	cout << "3. Show details of courses" << endl;
	cout << "4. Exit" << endl;
	cout << "Enter your choice: ";
	cin >> option;
	cout << endl;

	return option;

}

void printCourses(Course** list, int count)
{
	int i;
	Course* currCourse;
	int* gradesList;



	for (i = 0; i < count; ++i)
	{
		currCourse = list[i];

		// TO DO: shold get the course grades list into gradesList
		gradesList = currCourse->getGradesList();

		cout << "Name: " << currCourse->getName() << ", test1 = " << gradesList[0] << ", test2 = " << gradesList[1] << ", exam = " << gradesList[2] << ", Grade = " << currCourse->getFinalGrade() << endl;
	}
	cout << endl << endl;
}

int main()
{
	Student* student = NULL;
	double avg = 0;

	//Main Loop
	bool toExit = false;
	while (!toExit)
	{
		switch (showMenu())
		{
		case MENU_STUDENT_DETAILS:
			if (student)
			{
				delete student;
			}

			student = newStudent();

			break;
		case MENU_AVERAGE:
			if (!student)
				cout << "NO details were added" << endl;
			else
				cout << "The average grade of " << student->getCrsCount() << " courses is: " << student->getAvg() << endl << endl;
			break;
		case MENU_PRINT_COURSES:
			if (!student)
				cout << "NO details were added" << endl;
			else
			{
				cout << "Courses list for student " << student->getName() << endl;
				// TO DO: call the printCourses function (the function is implemented)
				printCourses(student->getCourses(), student->getCrsCount());
			}
			break;
		case MENU_EXIT:
			toExit = true;
			cout << "Goodbye!" << endl;
			break;
		default:
			cout << "No Such Option!" << endl << endl;
			break;
		}
	}

	if (student)
	{
		delete student;
	}
	system("pause");
	return 0;
}

